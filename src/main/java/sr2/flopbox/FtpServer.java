package sr2.flopbox;

import java.util.HashMap;
import java.util.ServiceConfigurationError;
import java.util.concurrent.atomic.AtomicLong;

/***
 * @author Nathan Houzet
 * Class that represent a FTP server and all its informations
 */

public class FtpServer {
    private final long id;
    private final String host;
    private final String port;
    private String alias;
    private HashMap<Integer,String> auth;
    private static final AtomicLong counter = new AtomicLong(100);

    /***
     * Constructor from a builder for the class
     * @param builder the FTP builder
     */
    private FtpServer(FtpServerBuilder builder){
        this.id = builder.id;
        this.host = builder.host;
        this.port = builder.port;
        this.alias = builder.alias;
        this.auth = new HashMap<Integer,String>();

    }

    /***
     * Constructor from a builder for the class
     */
    public FtpServer(){
        FtpServer ser = new FtpServer.FtpServerBuilder().id().build();
        this.id = ser.getId();
        this.host = ser.getHost();
        this.port = ser.getPort();
        this.alias = ser.getAlias();
        this.auth = new HashMap<Integer,String>();

    }

    /***
     * classic constructor for this class
     * @param id the if of the FTP server
     * @param host the host of the FTP server
     * @param port the port of the ftp server
     * @param alias the alias of the ftp server
     */
    public FtpServer(long id, String host, String port,String alias){
        FtpServer ser = new FtpServer.FtpServerBuilder().id()
                .host(host)
                .port(port)
                .alias(alias)
                .build();
        this.id = ser.getId();
        this.host = ser.getHost();
        this.port = ser.getPort();
        this.alias = ser.getAlias();
        this.auth = new HashMap<Integer,String>();

    }

    /***
     *
     * @return the if of the server
     */
    public long getId(){
        return this.id;
    }

    /***
     *
     * @return the host of the server
     */
    public String getHost() {
        return this.host;
    }

    /***
     *
     * @return the port of the server
     */
    public String getPort() {
        return this.port;
    }

    /***
     *
     * @return the alias of the server
     */
    public String getAlias() {return this.alias;}

    /**
     * Used to add a new user for this server
     * @param token the user token
     * @param user the user name
     * @param pass the user password
     */
    public void addUser(int token, String user, String pass){
        this.auth.put(token,user+";"+pass);
    }

    /***
     * check if an user exist for the server from his token
     * @param token the token to check
     * @return true if the token and so the user is know by this server
     *          else false
     */
    public boolean checkIfUserExist(int token){
        return this.auth.containsKey(token);
    }

    /***
     * Getter for the user name and password of a client
     * @param token the client's token that allow to find the credentials of the client
     * @return the user name and the password of the client user the form "user;password"
     */
    public String getUserAndPass(int token){
        return this.auth.get(token);
    }

    /***
     *
     * @return a string that describe the server
     */
    @Override
    public String toString(){
        return "ID: " + id
                + " Host: " + host
                + " Port: " + port
                + " Alias " + alias + "\n"
                ;
    }

    /***
     *
     * @param newAlias The new alias of the server
     */
    public void setAlias(String newAlias) {
        this.alias=newAlias;
    }

    /***
     * Constructor of a FTP client builder
     */
    public static class FtpServerBuilder{
        private long id;
        private String host = "";
        private String port = "";
        private String alias = "";

        /***
         *
         * @return a id incremented from 1 comparing to the last id given
         */
        public FtpServerBuilder id(){
            this.id = FtpServer.counter.getAndIncrement();
            return this;
        }

        /***
         *
         * @param id The id of the server
         * @return the id of the server
         */
        public FtpServerBuilder id(long id){
            this.id = id;
            return this;
        }

        /***
         *
         * @param host The host of the server
         * @return the host of the server
         */
        public FtpServerBuilder host(String host){
            this.host = host;
            return this;
        }

        /****
         *
         * @param port The port of the server
         * @return the port of the server
         */
        public FtpServerBuilder port(String port){
            this.port = port;
            return this;
        }

        /***
         *
         * @param alias The alias of the server
         * @return the alias of the server
         */
        public FtpServerBuilder alias(String alias){
            this.alias = alias;
            return this;
        }


        /***
         * Building method
         * @return a FTPServer
         */
        public FtpServer build(){
            return new FtpServer(this);
        }

    }
}
