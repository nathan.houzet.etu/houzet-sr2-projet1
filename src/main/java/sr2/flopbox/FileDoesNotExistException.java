package sr2.flopbox;

/***
 * @author Nathan Houzet
 * Exception for the case where the client want to retrieve a file that doent exist
 */
public class FileDoesNotExistException extends Exception{
    public FileDoesNotExistException(String errorMessage) {
        super(errorMessage);
    }
}
