
package sr2.flopbox;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.glassfish.grizzly.http.server.HttpServer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class Tests {

    FtpServer ser= new FtpServer.FtpServerBuilder().id().host("212.27.60.27").port("22").alias("free").build();
    FTPTool FTPtool = new FTPTool();

    /**
     * Test the token of a client is well generated.
     */

    @Test
    public void testTokenOK() {
        FTPTool FTPtool = new FTPTool();
        String tmp = "alias"+"nathan"+"houzet";
        assertEquals(tmp.hashCode(), FTPtool.createToken("alias","nathan","houzet") );
    }

    /**
     * Test the token of a client is well generated.
     */

    @Test
    public void testTokenKO() {

        String tmp = "alias"+"nathan"+"houze";
        assertFalse(tmp.hashCode()== FTPtool.createToken("alias","nathan","houzet") );
    }

    @Test
    public void testAuthKOBecauseNotregister(){
        int token = FTPtool.createToken("alias","nathan","houzet");
        assertFalse(ser.checkIfUserExist(token));
    }

    @Test
    public void testAuthKOBecauseNotGoodToken(){
        int token = FTPtool.createToken("alias","nathan","houzet");
        ser.addUser(token,"nathan", "houzet");
        assertFalse(ser.checkIfUserExist(token-1));
    }

    @Test
    public void testAuthOK(){
        int token = FTPtool.createToken("alias","nathan","houzet");
        ser.addUser(token,"nathan", "houzet");
        assertTrue(ser.checkIfUserExist(token));
    }

    @Test
    public void testGetterAuth(){
        int token = FTPtool.createToken("alias","nathan","houzet");
        ser.addUser(token,"nathan", "houzet");
        assertEquals("nathan;houzet",ser.getUserAndPass(token));
    }

    @Test
    public void testFTPServerGetter(){
        assertEquals("212.27.60.27",ser.getHost());
        assertEquals("22",ser.getPort());
        assertEquals("free",ser.getAlias());
    }

    @Test
    public void testFTPServerSetter(){
        assertEquals("free",ser.getAlias());
        ser.setAlias("toto");
        assertEquals("toto",ser.getAlias());
    }



}



