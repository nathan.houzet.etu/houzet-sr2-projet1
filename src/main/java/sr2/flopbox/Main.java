package sr2.flopbox;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;


/**
 * @author  Nathan Houzet
 * Main class.
 * Init the API and launch it
 */
public class Main {
    // Base URI the Grizzly HTTP server will listen on
    public static final FTPTool FTPtool = new FTPTool();

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     * @param BASE_URI
     */
    public static HttpServer startServer(String BASE_URI) {
        // create a resource config that scans for JAX-RS resources and providers
        // in sr2.flopbox package
        final ResourceConfig rc = new ResourceConfig().packages("sr2.flopbox");

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    /**
     * Main method.
     * @param args the arguments of the main : NONE here
     * @throws IOException if they are a problem while starting the HTTP server
     */
    public static void main(String[] args) throws IOException {
        String url;
        String port;
        if (args.length==2) {
             url = args[0];
             port = args[1];
        }
        else{
             url = "flopbox";
             port = "8080";
        }
        String BASE_URI = "http://localhost:"+port +"/"+url+"/";

        final HttpServer server = startServer(BASE_URI);
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl\nHit enter to stop it...", BASE_URI));
        try {
            System.in.read();
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }finally {
            server.stop();
        }

    }
}

