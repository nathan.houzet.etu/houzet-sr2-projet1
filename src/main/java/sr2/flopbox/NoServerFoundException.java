package sr2.flopbox;

/***
 * Exception throws if the alias dont refer to  server
 */
public class NoServerFoundException extends Exception {
    public NoServerFoundException(String errorMessage) {
        super(errorMessage);
    }
}
