package sr2.flopbox;


import org.apache.commons.io.IOUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;

/***
 * @author  Nathan Houzet
 * Second Ressource of the API
 * Its manage all files findind and displaying exept the root repository of a server
 */
@Path("servers/{alias}/files")
public class FilesServices {

    /***
     * Method to list the content of any repository
     * @param token the client token from the header
     * @param file the repository to display
     * @param alias the ftp server alias
     * @return The content of the `file` repository
     * @throws NoServerFoundException If the alias refer to no server known
     * @throws IOException if they are a FTP command fail during the function process
     * @throws FileDoesNotExistException is the repository doesnt exist
     */
    @GET
    @Path("getContentRep")
    @Produces(MediaType.TEXT_PLAIN)
    public String listrepository(@HeaderParam("token")String token, @QueryParam("file")String file, @PathParam("alias") String alias) throws NoServerFoundException, IOException, FileDoesNotExistException {
        if (!FTPTool.getFTServerFromAlias(alias).checkIfUserExist(Integer.parseInt(token))) {
            return "400 please authenticate yourself for the server" + alias + " via " + alias + "/login";
        }
        String res = FTPTool.listRepository(alias,Integer.parseInt(token),file);
        System.out.println(res);
        return res;
    }

    /***
     * Method to list the content of any text file
     * @param token the client token from the header
     * @param file the file to display
     * @param alias the ftp server alias
     * @return The content of the `file`
     * @throws NoServerFoundException If the alias refer to no server known
     * @throws IOException if they are a FTP command fail during the function process
     * @throws FileDoesNotExistException is the file doesnt exist
     */
    @GET
    @Path("getContentFile")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Object listFile(@HeaderParam("token")String token, @QueryParam("file")String file, @PathParam("alias") String alias) throws NoServerFoundException, IOException, FileDoesNotExistException {
        if (!FTPTool.getFTServerFromAlias(alias).checkIfUserExist(Integer.parseInt(token))){
            return "400 please authenticate yourself for the server"+ alias+" via " +alias+"/login";
        }
        byte[] res  = FTPTool.listFile(alias,Integer.parseInt(token),file);
        Response.ResponseBuilder builder = Response.ok(res);
        builder.header("Content-Disposition", "attachment; filename=" + file);

        return res;
    }

    /***
     * Method used to rename a file or a repository
     * @param token the client token from the header
     * @param path the path of the file to rename
     * @param alias the ftp server alias
     * @param newPath the new name of the repository
     * @return a String about the success or the fail of the method
     * @throws NoServerFoundException If the alias refer to no server known
     * @throws IOException if they are a FTP command fail during the function process
     * @throws FileDoesNotExistException is the file doesnt exist
     */
    @POST
    @Path("rename")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_OCTET_STREAM})
    public String rename(
                            @HeaderParam("token")String token,
                            @QueryParam("path")String path,
                            @PathParam("alias") String alias,
                            @QueryParam("newPath") String newPath) throws NoServerFoundException, IOException, FileDoesNotExistException {

        if (!FTPTool.getFTServerFromAlias(alias).checkIfUserExist(Integer.parseInt(token))){
            return "400 please authenticate yourself for the server"+ alias+" via " +alias+"/login";
        }
        Boolean res = FTPTool.renameFile(alias,Integer.parseInt(token),path,newPath);

        return res? "200 File well renamed": "400 Failed to rename that File";
    }

    /***
     * method used to delete any file or repository of the server 'alias'
     * @param token the client token from the header
     * @param path the path of the file to delete
     * @param alias the ftp server alias
     * @return  a String about the success or the fail of the method
     * @throws NoServerFoundException If the alias refer to no server known
     * @throws IOException if they are a FTP command fail during the function process
     */
    @DELETE
    @Path("delete")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_OCTET_STREAM})
    public Object delete(
            @HeaderParam("token")String token,
            @QueryParam("path")String path,
            @PathParam("alias") String alias
            ) throws NoServerFoundException, IOException {

        if (!FTPTool.getFTServerFromAlias(alias).checkIfUserExist(Integer.parseInt(token))){
            return "400 please authenticate yourself for the server"+ alias+" via " +alias+"/login";
        }
        Boolean res = FTPTool.deleteFile(alias,Integer.parseInt(token),path);

        return res? "200 File well deleted": "400 Failed to deleted that File";
    }

    /***
     * Method used to create repository
     * @param token the client token from the header
     * @param path the path of the repository to create
     * @param alias the ftp server alias
     * @return a String about the success or the fail of the method
     * @throws NoServerFoundException If the alias refer to no server known
     * @throws IOException if they are a FTP command fail during the function process
     */
    @PUT
    @Path("add")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_OCTET_STREAM})
    public Object addfile(
            @HeaderParam("token")String token,
            @QueryParam("path")String path,
            @PathParam("alias") String alias
    ) throws NoServerFoundException, IOException {

        if (!FTPTool.getFTServerFromAlias(alias).checkIfUserExist(Integer.parseInt(token))){
            return "400 please authenticate yourself for the server"+ alias+" via " +alias+"/login";
        }
        Boolean res = FTPTool.addFile(alias,Integer.parseInt(token),path);

        return res? "200 File well created": "400 Failed to create that File";
    }

    /***
     * Method to check is the resource is ok
     * @param token the client token
     * @param alias the ftp server alias
     * @return a string about the resource
     * @throws NoServerFoundException is the alias doesnt refer to a ftp server known
     */
    @GET
    @Produces({MediaType.TEXT_PLAIN})
    public String test( @HeaderParam("token")String token,
                        @PathParam("alias") String alias) throws NoServerFoundException {

        if (!FTPTool.getFTServerFromAlias(alias).checkIfUserExist(Integer.parseInt(token))) {
            return "400 please authenticate yourself for the server" + alias + " via " + alias + "/login";
        }
        return "200 you're inn the file resource!!";
    }



}
