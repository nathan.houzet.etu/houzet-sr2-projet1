package sr2.flopbox;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import  javax.ws.rs.QueryParam;
import javax.validation.constraints.NotNull;


/***
 * @author  Nathan Houzet
 * Main ressource of the API
 * Its manage the server part
 */
@Path("/servers")
public class FtpServerService {

    private final CopyOnWriteArrayList<FtpServer> servList = FtpServerList.getInstance();


    /***
     * Method used to add a server to the API
     * @param host The hst name of the new server
     * @param port the port of the new server
     * @param alias the alias of the new server
     * @return A string about the success of the adding
     */
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String addServer(@NotNull @QueryParam("host") String host, @NotNull @QueryParam("port") String port,@NotNull @QueryParam("alias") String alias){
        if(host==null || port==null|| alias ==null)
            return "400 an empty host or an empty port or an empty alias is given";
        else {
            servList.add(new FtpServer.FtpServerBuilder().id().host(host).port(port).alias(alias).build());
            return "200 server " + host + " on the port " + port + " is well add to the flopbox";
        }
    }

    /***
     * method that return all the FTP server know by the API
     * @return A string about each servers
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getAllFTPServers() {
        return "---FTP Server---\n"
                + servList.stream()
                .map(c -> c.toString())
                .collect(Collectors.joining("\n"));
    }

    /***
     * Method that gives information about one FTP server
     * @param alias the alias of the server
     * @return information about the FTP server
     */
    @GET
    @Path("{alias}/info")
    @Produces(MediaType.TEXT_PLAIN)
    public String getServer(@PathParam("alias") String alias) {
        Optional<FtpServer> match
                = servList.stream()
                .filter(c -> c.getAlias().equals(alias))
                .findFirst();
        if (match.isPresent()) {
            return "---FTP Server---\n" + match.get().toString();
        } else {
            return "Server not found";
        }
    }

    /***
     * Method the register a username and a password for the server 'alias'
     * The credentials will be only save for this server
     * @param user the username of the client
     * @param pass the password of the client
     * @param alias the alias of the FTP server
     * @return a token that the client will have to send in each request in the header
     * @throws NoServerFoundException if the alias doesnt refer to a known server
     */
    @GET
    @Path("{alias}/login")
    @Produces(MediaType.TEXT_PLAIN)
    public String login(@QueryParam("user") String user,@QueryParam("pass") String pass,@PathParam("alias")String alias) throws NoServerFoundException {
        boolean error = FTPTool.CheckAuthentication(alias,user,pass);
        if (!error ){
            int token = FTPTool.createToken(alias,user,pass);
            FTPTool.getFTServerFromAlias(alias).addUser(token,user,pass);
            return "200 Authentication to "+alias+" is done, your token to put in " +
                    "header is "+token;
        }else{
            return "400 the server don't recognize these credentials";
        }

    }

    /***
     * Method to get the content of the root repertory  of a server
     * @param token the client connexion token
     * @param alias the FTP server alias
     * @return The content of the root of the repertory
     * @throws NoServerFoundException  if the alias doesnt refer to a known server
     * @throws IOException If a FTP command fails in the execution
     * @throws cantConnectToServerException if they are a problem of connection with the server
     */
    @GET
    @Path("{alias}")
    @Produces(MediaType.TEXT_PLAIN)
    public String listRoot(@HeaderParam("token")String token,@PathParam("alias") String alias) throws NoServerFoundException, IOException, cantConnectToServerException {
        if (!FTPTool.getFTServerFromAlias(alias).checkIfUserExist(Integer.parseInt(token))){
            return "400 please authenticate yourself for the server"+ alias+" via " +alias+"/login";
        }
        return FTPTool.listRoot(alias, Integer.parseInt(token));

    }

    /***
     * Method to change a alias of a FTP server
     * @param token the client connexion token
     * @param alias the alias of the server to change the alias
     * @param newAlias the new alias
     * @return a String about the success of the function
     * @throws NoServerFoundException if the alias doesnt refer to a server known
     */
    @PUT
    @Path("{alias}/change")
    @Produces(MediaType.TEXT_PLAIN)
    public String changeAlias(@HeaderParam("token")String token,@PathParam("alias") String alias,@QueryParam("newAlias") String newAlias) throws NoServerFoundException{
        if (!FTPTool.getFTServerFromAlias(alias).checkIfUserExist(Integer.parseInt(token))){
            return "400 please authenticate yourself for the server"+ alias+" via " +alias+"/login";
        }
        FtpServer ser = FTPTool.getFTServerFromAlias(alias);
        ser.setAlias(newAlias);
        return "200 alias was well changed in "+newAlias + " for the server "+ser.getHost();

    }

    /***
     * Method used to remove a server to the API
     * @param alias the alias of the new server
     * @return A string about the success of the deleting
     * @throws NoServerFoundException If the alias dont refer to a server
     */
    @DELETE
    @Path("{alias}")
    @Produces(MediaType.TEXT_PLAIN)
    public String addServer(@PathParam("alias") String alias) throws NoServerFoundException {
            servList.remove(FTPTool.getFTServerFromAlias(alias));
            return "200 server  is well deleting from the flopbox";
        }


}
