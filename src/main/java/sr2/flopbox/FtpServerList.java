package sr2.flopbox;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

/***
 * @author  Nathan Houzet
 * Class that contain the servers known by the API
 */
public class FtpServerList {
    private static final CopyOnWriteArrayList<FtpServer> sList = new CopyOnWriteArrayList<>();

    static {
        // Create list of customers
        sList.add(
                new FtpServer.FtpServerBuilder().id()
                        .host("212.27.60.27")
                        .port("22")
                        .alias("free")
                        .build()
        );

        sList.add(
                new FtpServer.FtpServerBuilder().id()
                        .host("91.189.88.152")
                        .port("22")
                        .alias("ubuntu")
                        .build()
        );




    }

    /***
     * @return an instance of the servers list
     */
    public static CopyOnWriteArrayList<FtpServer> getInstance(){
        return sList;
    }


}
