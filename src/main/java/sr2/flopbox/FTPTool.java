package sr2.flopbox;

import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import javax.ws.rs.core.Response;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Nathan Houzet
 * Class used as a tool box
 * It is used to communicate with the ftp client
 * And contain also other usefull methods.
 */

class FTPTool {

    private static final CopyOnWriteArrayList<FtpServer> servList = FtpServerList.getInstance();

    /***
     * Verifie is the user is well now by the server
     * @param alias the ftp server alias
     * @param user the username of the client
     * @param pass the password of the client
     * @return true if the client is know by the server, false else
     * @throws NoServerFoundException if the alias doesnt refer to a server
     */
    public static boolean CheckAuthentication(String alias, String user, String pass) throws NoServerFoundException {
        FTPClient ftp = new FTPClient();
        FtpServer ftpServ = getFTServerFromAlias(alias);
        boolean error = false;
        try {
            int reply;
            ftp.connect(ftpServ.getHost(),Integer.parseInt(ftpServ.getPort()));
            System.out.println("Connected to " + ftpServ.getHost() + ".");
            System.out.print(ftp.getReplyString());

            // After connection attempt, you should check the reply code to verify
            // success.
            reply = ftp.getReplyCode();

            if (!FTPReply.isPositiveCompletion(reply)) {
                ftp.disconnect();
                System.err.println("FTP server refused connection.");
                System.exit(1);
            }

            boolean success = ftp.login(user, pass);
            reply = ftp.getReplyCode();
            if (!success) {
                System.out.println("Could not login to the server");
                return false;
            }

            ftp.logout();
        } catch (IOException e) {
            error = true;
        }
        return error;
    }

    /***
     * Method the return the FTPServer from its alias
     * @param alias the alias of the FTP server
     * @return The FTP server corresponding to the alias
     * @throws NoServerFoundException If the alias is not know
     */
    public static FtpServer getFTServerFromAlias(String alias) throws NoServerFoundException {
        Optional<FtpServer> match
                = servList.stream()
                .filter(c -> c.getAlias().equals(alias))
                .findFirst();
        if (match.isPresent()) {
            return match.get();
        }else{
            throw  new NoServerFoundException("Error, cant find the server called "+alias);
        }
    }

    /***
     * Method used to list the root of a ftp server
     * @param alias the alias of the server
     * @param token the connexion token of the client
     * @return the content of the root directory
     * @throws NoServerFoundException is the alias given doesnt represent a know server
     */
    public static String listRoot(String alias,int token) throws NoServerFoundException {
        String res = null;
        try {
            FTPClient ftp = FTPTool.ConnectAndLogIn(alias,token);
            // list files of the server
            FTPFile[] files = ftp.listFiles();

            res = ArrangeListing(files);

            ftp.logout();
            return res;
        } catch (IOException e) {
            res = e.toString();

        }
        return res;
    }

    /***
     * Method used to well print the content of a directory
     * @param files a array of FTPFiles
     * @return a nice string representing the content
     */
    private static String ArrangeListing(FTPFile[] files) {
        String res=null;
        DateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // iterates over the files and prints details for each
        for (FTPFile file : files) {
            String details = file.getName();
            if (file.isDirectory()) {
                details = "[" + details + "]";
            }
            details += "\t\t" + file.getSize();
            details += "\t\t" + dateFormater.format(file.getTimestamp().getTime());
            res += details + "\n";
        }
        return res;
    }

    /***
     * Create a connexion token from the user name, the password and the alias of the server
     * @param alias the alias of the server
     * @param user the user name of the client
     * @param pass the password of the client
     * @return a connexion token
     */
    public static int createToken(String alias, String user, String pass) {
        return (alias+user+pass).hashCode();
    }

    /***
     * Method to retrieve the content of anyone repository
     * @param alias the alias of the FTP server
     * @param token the client token
     * @param file the repository to see the content
     * @return The repository description
     * @throws NoServerFoundException
     * @throws IOException
     * @throws FileDoesNotExistException
     */
    public static String listRepository(String alias, int token, String file) throws NoServerFoundException, IOException, FileDoesNotExistException {
        FTPClient ftp = FTPTool.ConnectAndLogIn(alias,token);
        String res;

        //on avance de dossier en dossier
        String [] tab = file.split("/");
        for (int i = 0;i<tab.length-1;i++){
            ftp.changeWorkingDirectory(tab[i]);
            int returnCode = ftp.getReplyCode();
            if (returnCode == 550) {
                throw new FileDoesNotExistException("Error, cant find the file called "+tab[i]+"on the server"+alias);
            }

        }
        // on récupère le contenu du repertoire du dernier répertoire
        ftp.changeWorkingDirectory(tab[tab.length-1]);
        int returnCode = ftp.getReplyCode();

        if (!FTPReply.isPositiveCompletion(returnCode)) {
            ftp.disconnect();
            System.exit(1);
        }
        FTPFile[] files = ftp.listFiles();
        res = ArrangeListing(files);

        ftp.logout();
        return res;

    }
    /***
     * Method to retrieve the content of anyone text file
     * @param alias the alias of the FTP server
     * @param token the client token
     * @param file the file to see the content
     * @return a array of byte with the file content
     * @throws NoServerFoundException if the alias is not linked to a server
     * @throws IOException If they are a problem while the classic ftp commands
     * @throws FileDoesNotExistException If the file given in parameter doesnt exist
     */
    public static byte[] listFile(String alias, int token, String file) throws NoServerFoundException, IOException, FileDoesNotExistException {
        FTPClient ftp = FTPTool.ConnectAndLogIn(alias,token);

        InputStream inputStream = ftp.retrieveFileStream(file);
        int returnCode = ftp.getReplyCode();
        if (returnCode == 550) {
            throw new FileDoesNotExistException("Error, cant find the file called "+file+"on the server "+alias);
        }
        byte[] bytes = IOUtils.toByteArray(inputStream);
        ftp.logout();
        return bytes;

    }

    /***
     * Method used to rename an file or a directory from a ftp server.
     * @param alias the alias of the ftp server
     * @param token the connexion token of the client
     * @param path the path of the file to be deleted
     * @param newPath the new name of the file
     * @return  true if the file is deleted
     * @throws IOException if they are problem with the ftp server connection like a bad port or host
     * @throws NoServerFoundException if the alias dont represent a server.
     */
    public static Boolean renameFile(String alias, int token, String path,String newPath) throws NoServerFoundException, IOException {

        System.out.println("in Connect and test");
        FTPClient ftp = FTPTool.ConnectAndLogIn(alias,token);

        ftp.rename(path,newPath);
        int reply = ftp.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            ftp.disconnect();
            System.err.println("FTP server refused to rename.");
            return false;
        }
        ftp.logout();
        ftp.disconnect();
        return true;
    }


    /***
     * Method used to init the ftp connection and the logging the client.
     * avoid to repeat it in all the ftp commands functions
     * @param alias the alias of the ftp server
     * @param token the connexion token of the client
     * @return The Ftp client created and logged in
     * @throws NoServerFoundException if they are problem with the ftp server connection like a bad port or host
     * @throws IOException if the alias dont represent a server.
     */
    public static FTPClient ConnectAndLogIn(String alias, int token) throws NoServerFoundException, IOException {

        FTPClient ftp = new FTPClient();
        FtpServer ftpServ = getFTServerFromAlias(alias);
        int reply;


        ftp.connect(ftpServ.getHost(), Integer.parseInt(ftpServ.getPort()));
        System.out.println("Connected to " + ftpServ.getHost() + ".");
        System.out.print(ftp.getReplyString());
        String user = ftpServ.getUserAndPass(token);
        String [] tmp= user.split(";");
        user = tmp[0];
        String pass = tmp[1];

        // After connection attempt, you should check the reply code to verify
        // success.
        reply = ftp.getReplyCode();

        if (!FTPReply.isPositiveCompletion(reply)) {
            ftp.disconnect();
            System.err.println("FTP server refused connection.");
            System.exit(1);
        }
        boolean success = ftp.login(user, pass);
        reply = ftp.getReplyCode();

        if (!success) {
            System.err.println(reply+"Could not login to the server");
            System.exit(1);
        }
        return ftp;
    }

    /***
     * Method used to delete an file or a directory from a ftp server.
     * The program will firstly try to remove the file like a file and if its fails to remove it like a directory and else fail.
     * @param alias the alias of the ftp server
     * @param token the connexion token of the client
     * @param path the path of the file to be deleted
     * @return  true if the file is deleted
     * @throws IOException if they are problem with the ftp server connection like a bad port or host
     * @throws NoServerFoundException if the alias dont represent a server.
     */
    public static Boolean deleteFile(String alias, int token, String path) throws IOException, NoServerFoundException {
        FTPClient ftp = FTPTool.ConnectAndLogIn(alias,token);

        ftp.deleteFile(path);
        int reply = ftp.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            ftp.removeDirectory(path);
            reply = ftp.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                System.err.println("FTP server refused to rename.");
                return false;
            }
        }
        ftp.logout();
        return true;
    }

    /***
     * Method used to add a new directory to the FTP server
     * @param alias the alias of the FTP server
     * @param token the client token
     * @param path the path of the new file to add
     * @return  true if the file is well created else false
     * @throws IOException if the mkd ftp comment fail
     * @throws NoServerFoundException is the server named by the alias doesnt exist
     */
    public static Boolean addFile(String alias, int token, String path) throws IOException, NoServerFoundException {
        FTPClient ftp = FTPTool.ConnectAndLogIn(alias,token);

        ftp.makeDirectory(path);
        int reply = ftp.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            System.err.println("FTP server refused to create the file.");
            return false;
        }
        ftp.logout();
        return true;
    }

}