API Flopbox
Nathan Houzet  
2021

# Introduction 

Développer la plate-forme FlopBox en adoptant le style architectural REST pour vous permettre de centraliser la gestion de vos fichiers distants stockés dans des serveurs FTP tiers.

Vous utiliserez notamment JAX-RS (ou tout autre équivalent) pour développer cette plate-forme.

Vous proposerez un mécanisme d'authentification adéquat tenant compte des différentes clés d'authentification envisageables (celles des différents serveurs FTP auquel on peut avoir accès, ainsi que celles de la plate-forme FlopBox).

# Lancer le programme

Une [démo du programme](./doc/Demo.mp4) est disponible dans le dossier doc.
La démo illustre toutes les fonctionnalités implémentées

Pour lancer le serveur il faut : 
 - se placer à la racine du projet dans le dossier "flopboxProject"
 - executer la commande `mvn package` ( si tout est bien configuré, un dossier target avec un executable "flopbox-exe-1.0-jar-with-dependencies.jar" sont générés )
 - executer le jar avec la commande `java -jar target/flopbox-exe-1.0-jar-with-dependencies.jar`
 - il est possible de choisir son URL de base et son port en spécifiant `java -jar 
   target/flopbox-exe-1.0-jar-with-dependencies.jar <url> <port>`
   `
   
Une copie de l'archive Jar est disponible dans le dossier doc.
Votre server Flopbox est ainsi lancé, il attend sagement des requêtes à éxecuter.
   
# Liste des requetes : 

Je précise que comme je n'étais pas du tout habitué à Curl, j'ai fait 
tout mes requêtes sur Postman. De plus j'ai utilisé Postman car en essayant avec Curl 
certaines requetes au début, elles ne fonctionnaient pas alors que sur Postman oui.
Je ne sais donc pas du tout si ces requêtes vont fontionner sur Curl ...

NB : Si vous souhaitez importer [mes requetes](.doc/flopbox.postman_collection.json) de la démo sous postman, elle sont 
disponibles dans le dossier doc. 

Vérifier la liste des serveurs que contient Flopbox (contient déjà free et ubuntu) :

 * `GET localhost:8080/flopbox/servers`

Ajouter un server : 

 * `POST localhost:8080/flopbox/servers?host=127.0.0.1&port=2121&alias=perso`

Ajouter un identifiant et un mot de passe pour le server précédent : 

 * `GET localhost:8080/flopbox/servers/perso/login?user=nathan&pass=houzet`
Renvoie un 'token' de connexion. Ne pas oublier de le mettre dans le header avec un 
   onglet 'token' pour toute les commandes suivantes.
   
Afficher la racine d'un répertoire : 

 * `GET localhost:8080/flopbox/servers/perso`

Supprimer un fichier ou dossier : 
 
 * `DEL localhost:8080/flopbox/servers/perso/files/delete?path=Desktop/toto.txt`

Ajouter un dossier : 

* `PUT localhost:8080/flopbox/servers/perso/files/add?path=Desktop/toto`

Renommer un fichier ou un dossier : 

* `POST localhost:8080/flopbox/servers/perso/files/rename?path=Desktop/toto.txt&newPath=Desktop/tata.txt`

Récuperer le contenu d'un répertoire : 

* `GET localhost:8080/flopbox/servers/perso/files/getContentRep/?file=Desktop/website`

Récuperer le contenu d'un fichier texte : 

* `GET localhost:8080/flopbox/servers/perso/files/getContentFile/?file=Desktop/memoire.txt`

Modifier un alias : 

* `PUT localhost:8080/flopbox/servers/perso/change?newAlias=perso2`

Supprimer un server de la plateforme :

* `DEL localhost:8080/flopbox/servers/perso`

A noter que ici 'perso' est l'alias que j'ai utilisé pour mon serveur local de test. 


# Architecture

Le programme est principalement composé de 5 classes.



**Une classe FTPServerService** : 

Cette classe représente la première ressource `server`. Elle permet d'ajouter les 
serveurs, afficher les infos sur les serveurs et de changer les alias principalement

**Une classe FilesServices** : 

Cette classe réprésente la ressource fichier/dossier. Elle permet de les modifiers, 
les supprimer, les effacers. Elle permet aussi d'afficher les contenus des répertoires 
et de affichier les fichier textes. 

NB : L'upload et le download de fichiers binaires n'as pas été implémenté.

**Une classe FtpServer** :

Cette classe représente un server FTP et donc contient tout les infos utiles à son 
fonctionnement comme l'hote, le port, l'alias mais aussi les liste des utilisateurs 
associés et leurs token de connexion respectifs.

**Une classe FTPTool** :

Cette classe est une classe outil permettant de gérer les connexions ftp avec serveurs
FTP via la classe FTPClient.
Elle gère également les authentifications des clients.

**Une classe FtpServerList** :

Cette classe est principalement la liste des serveurs FTP connus par la plateforme 
Flopbox.

Diagrammes UML des classes : 

![Classes crées.](./doc/flopbox.png)

**La classe Main** : 

Cette classe permet de lancer le service flopbox et puis attends les requêtes.

**Les exceptions** : 

De plus, 3 exceptions ont été créés pour préciser les différentes problèmes qu'il peut y 
avoir lors de l'éxecution du programme : 

- Une exception qui concerne les alias, si un alias n'est associé à aucun serveur, alors 
  l'exception 
  "NoServerFoundException" est levé.

- Une exception par rapport aux fichiers des serveurs FTP. Si un fichier est introuvable 
  sur un server, alors l'exception 
  "FileDoesNotExistException" est levé.
  
- La dernière, la connection au serveurs FTP. Si une connection échoue, alors 
  l'exception "cantConnectToServerException" est déclenché.
 

# Parcours de code

**Code 1**
```java
new FtpServer.FtpServerBuilder().id()
        .host("91.189.88.152")
        .port("22")
        .alias("ubuntu")
        .build()
```

Pour créer et ajouter dynamiquement mes objets FTPServer à la liste des servers connus 
par la plateforme, j'utilise le concept COO de builder.


**Code 2**
```java
 public static FTPClient ConnectAndLogIn(String alias, int token)
```

La méthode `ConnectAndLogIn` de la classe FTPTool. Cette méthode va initialiser une 
connexion FTP et également connecter le client à partir de son token et de l'alias 
utilisé. Elle est très pratique car c'est une étape répétitive pour quasiment chaque 
services de l'API. Elle regroupe le code commun de plusieurs méthodes.




# Tests

Quelques tests ont été réalisé sur les méthodes ne nécéssitant pas d'avoir des clients 
qui envoient des requetes et des serverurs FTP en route pour y répondre.

![Tests crées.](./doc/Tests.png)

# Documentation

La documentation est disponible dans le dossier doc via le fichier [*index.html*](./doc/index.html). 