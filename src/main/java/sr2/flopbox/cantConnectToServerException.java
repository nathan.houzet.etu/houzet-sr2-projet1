package sr2.flopbox;

/***
 * @author  Nathan Houzet
 * Exception for the case where the client can't connect to the server
 */
public class cantConnectToServerException extends Exception{
    public cantConnectToServerException(String errorMessage) {
        super(errorMessage);
    }
}
